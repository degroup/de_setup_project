""" unit tests for de.setup_project portion. """
import os
import pytest
import setuptools
import shutil

# noinspection PyUnresolvedReferences
from de.setup_project import (
    APP_PRJ, MODULE_PRJ, NO_PRJ, PACKAGE_PRJ, PARENT_PRJ, ROOT_PRJ, PEV_DEFAULTS_SECTION_NAME,
    PYPI_PROJECT_ROOT, REPO_CODE_DOMAIN, REPO_GROUP_SUFFIX, REQ_FILE_NAME, REQ_DEV_FILE_NAME,
    STK_AUTHOR_EMAIL, PARENT_FOLDERS,
    code_file_title, code_file_version, find_data_files, find_resources, namespace_guess,
    pev_str, pev_val, project_env_vars)

from ae.base import (
    BUILD_CONFIG_FILE, CFG_EXT, DOCS_FOLDER, INI_EXT, PY_EXT, PY_INIT, TEMPLATES_FOLDER, TESTS_FOLDER,
    norm_path, write_file)
from ae.paths import PATH_PLACEHOLDERS
from ae.console import ConsoleApp


class TestHelpers:
    """ test helper functions """
    def test_code_file_title(self):
        tst_file = 'test_code_title.py'
        title_str = "this is an example of a code file title string"

        try:
            write_file(tst_file, f'''""" {title_str}\n\n    docstring body start here..."""\n''')
            assert code_file_title(tst_file) == title_str

            write_file(tst_file, f'''"""\n{title_str}\n====================\n    docstring body start here..."""\n''')
            assert code_file_title(tst_file) == title_str
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_title_invalid_file_content(self):
        tst_file = 'test_code_title.py'
        try:
            write_file(tst_file, "")                       # empty file
            assert not code_file_title(tst_file)

            write_file(tst_file, "\n\n this is no docstring and no title")    # invalid docstring/title
            assert not code_file_title(tst_file)
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_title_invalid_file_name(self):
        assert not code_file_title('::invalid_file_name::')

    def test_code_file_version(self):
        tst_file = 'test_code_version.py'
        version_str = '33.22.111pre'
        try:
            write_file(tst_file, f"__version__ = '{version_str}'  # comment\nversion = '9.6.3'")
            assert code_file_version(tst_file) == version_str
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_version_invalid_file_content(self):
        tst_file = 'test_code_version.py'
        try:
            write_file(tst_file, "")                       # empty file
            assert not code_file_version(tst_file)

            write_file(tst_file, "version__ = '1.2.3'")    # invalid version var prefix
            assert not code_file_version(tst_file)
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_version_invalid_file_name(self):
        assert not code_file_version('::invalid_file_name::')

    def test_find_data_files(self):
        package_name = 'test_pkg_with_data_files'
        pkg_path = os.path.join(TESTS_FOLDER, package_name)
        deeper_dir = 'deeper_dir'
        tst_prj_root_file = os.path.join(pkg_path, 'prj_root.ext')
        path1 = os.path.join(pkg_path, DOCS_FOLDER)
        file1 = os.path.join(path1, '.dot_doc_data1.ext')
        path2 = os.path.join(path1, deeper_dir)
        file2 = os.path.join(path2, 'doc_data2.ext')
        file2d = os.path.join(path2, '.dot_doc_data2.ext')
        path3 = os.path.join(pkg_path, TEMPLATES_FOLDER)
        file3 = os.path.join(path3, 'dotLess.ext')
        file3d = os.path.join(path3, '.dot')
        try:
            os.makedirs(path2)
            os.makedirs(path3)
            write_file(tst_prj_root_file, "what ever")
            write_file(file1, "some data file content")
            write_file(file2, "data content2")
            write_file(file2d, "dot file data content2")
            write_file(file3, "data content3")
            write_file(file3d, "dot file data content3")

            dir_files = find_data_files({'project_path': pkg_path})
            assert len(dir_files) == 4
            assert ('', (os.path.relpath(tst_prj_root_file, pkg_path), )) in dir_files
            assert (DOCS_FOLDER, (os.path.relpath(file1, pkg_path), )) in dir_files
            found = False
            for rel_path, files in dir_files:
                if rel_path == os.path.join(DOCS_FOLDER, deeper_dir):
                    assert not found, f"{rel_path} found more than once"
                    found = True
                    assert len(files) == 2
                    assert os.path.relpath(file2, pkg_path) in files
                    assert os.path.relpath(file2d, pkg_path) in files
            assert found, f"{os.path.relpath(file2, pkg_path)} and {os.path.relpath(file2d, pkg_path)} not found"
            found = False
            for rel_path, files in dir_files:
                if rel_path == TEMPLATES_FOLDER:
                    assert not found, f"{rel_path} found more than once"
                    found = True
                    assert len(files) == 2
                    assert os.path.relpath(file3, pkg_path) in files
                    assert os.path.relpath(file3d, pkg_path) in files
            assert found, f"{os.path.relpath(file3, pkg_path)} and {os.path.relpath(file3d, pkg_path)} not found"
        finally:
            if os.path.exists(pkg_path):
                shutil.rmtree(pkg_path)

    def test_find_data_de_setup_project(self):
        data_files = find_data_files({'project_path': os.getcwd()})
        assert len(data_files) == 1
        assert data_files[0][0] == ""
        assert 'setup' + PY_EXT in data_files[0][1]

    def test_find_resources(self):
        package_name = 'test_pkg_with_resources'
        pkg_path = os.path.join(TESTS_FOLDER, package_name)
        tst_pgk_file = os.path.join(pkg_path, PY_INIT)
        path1 = os.path.join(pkg_path, 'img')
        file1 = os.path.join(path1, 'res.ext')
        path2 = os.path.join(path1, 'res_deep')
        file2 = os.path.join(path2, 'res2.ext')
        file3 = os.path.join(pkg_path, 'widget.kv')
        try:
            os.makedirs(path2)
            write_file(tst_pgk_file, "v = 3")
            write_file(file1, "some resource content")
            write_file(file2, "res content2")
            write_file(file3, "kv language content")

            files = find_resources(pkg_path)
            assert len(files) == 3
            assert files[0] == os.path.relpath(file3, pkg_path)
            assert files[1] == os.path.relpath(file1, pkg_path)
            assert files[2] == os.path.relpath(file2, pkg_path)
        finally:
            if os.path.exists(pkg_path):
                shutil.rmtree(pkg_path)

    def test_namespace_guess_fail(self):
        assert namespace_guess({}) == ""
        assert namespace_guess({'project_path': TESTS_FOLDER}) == ""
        assert namespace_guess({'project_path': "xy_portion"}) == ""

    def test_namespace_guess_root(self):
        namespace = 'xz'
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        project_path = os.path.join(parent_dir, namespace)
        docs_path = os.path.join(project_path, DOCS_FOLDER)
        template_path = os.path.join(project_path, TEMPLATES_FOLDER)
        try:
            os.makedirs(docs_path)
            os.makedirs(template_path)

            assert namespace_guess({'project_path': project_path}) == namespace
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_namespace_guess_portion(self):
        namespace = 'yz'
        portion_name = 'portion_name'
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        project_path = os.path.join(parent_dir, namespace + "_" + portion_name)
        namespace_dir = os.path.join(project_path, namespace)
        try:
            os.makedirs(namespace_dir)

            assert namespace_guess({'project_path': project_path}) == namespace
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    # noinspection PyTypeChecker
    def test_pev_str(self):
        """ test helper to determine variable string values of a project env variable. """
        assert pev_str({'a': 1}, "b") == ""
        assert pev_str(dict(a=1), "b") == ""
        with pytest.raises(AssertionError):
            assert pev_str(dict(a=1), "a") == ""
        assert pev_str(dict(a="1"), "a") == "1"

    # noinspection PyTypeChecker
    def test_pev_val(self):
        """ test helper to determine variable values of a project env var. """
        assert pev_val(dict(a=1), "b") == ""
        assert pev_val(dict(a=1), "a") == 1


class TestProjectEnvVars:
    """ test project_env_vars() function """
    def test_module_var_patch(self):
        # noinspection PyGlobalUndefined
        global REQ_FILE_NAME
        assert REQ_FILE_NAME == 'requirements.txt'
        REQ_FILE_NAME = 'new_val'
        assert REQ_FILE_NAME == 'new_val'

        REQ_FILE_NAME = 'requirements.txt'  # reset de.setup_project-module-var-value for subsequent tests

    def test_module_var_patch_local_imported(self):
        # noinspection PyGlobalUndefined
        global REQ_FILE_NAME
        import de.setup_project as dc
        assert dc.REQ_FILE_NAME == 'requirements.txt'
        dc.REQ_FILE_NAME = 'new_val'
        assert dc.REQ_FILE_NAME == 'new_val'
        assert REQ_FILE_NAME == 'requirements.txt'

        dc.REQ_FILE_NAME = 'requirements.txt'  # reset de.setup_project-module-var-value for subsequent tests

    def test_module_var_patch_imported_in_other_module(self):
        # noinspection PyGlobalUndefined
        global REQ_FILE_NAME
        write_file('other_module.py', "import de.setup_project as dc")
        # noinspection PyUnresolvedReferences
        from other_module import dc
        assert dc.REQ_FILE_NAME == 'requirements.txt'
        dc.REQ_FILE_NAME = 'new_val'
        assert dc.REQ_FILE_NAME == 'new_val'
        assert REQ_FILE_NAME == 'requirements.txt'
        os.remove('other_module.py')

        dc.REQ_FILE_NAME = 'requirements.txt'  # reset de.setup_project-module-var-value for subsequent tests

    def test_module_var_patch_imported_in_other_module_as(self):
        # noinspection PyGlobalUndefined
        global REQ_FILE_NAME
        write_file('other_module.py', "from de.setup_project import REQ_FILE_NAME")
        # noinspection PyUnresolvedReferences
        from other_module import dc
        assert dc.REQ_FILE_NAME == 'requirements.txt'
        dc.REQ_FILE_NAME = 'new_val'
        assert dc.REQ_FILE_NAME == 'new_val'
        assert REQ_FILE_NAME == 'requirements.txt'
        os.remove('other_module.py')

        dc.REQ_FILE_NAME = 'requirements.txt'  # reset de.setup_project-module-var-value for subsequent tests

    def test_app_env(self):
        file_name = os.path.join(TESTS_FOLDER, BUILD_CONFIG_FILE)
        try:
            write_file(file_name, "spec")
            pev = project_env_vars(project_path=TESTS_FOLDER)
            assert pev['namespace_name'] == ""
            assert pev['package_name'] == TESTS_FOLDER
            assert pev['project_type'] == APP_PRJ
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_de_project_env_vars(self):
        namespace = 'de'
        module = 'setup_project'
        cur_dir = os.getcwd()

        pev = project_env_vars(from_setup=True)

        assert isinstance(pev, dict)
        assert pev['namespace_name'] == namespace
        assert pev['TEMPLATES_FOLDER'] == TEMPLATES_FOLDER == 'templates'
        assert pev['PYPI_PROJECT_ROOT'] == PYPI_PROJECT_ROOT == 'https://pypi.org/project'
        assert pev['REPO_CODE_DOMAIN'] == pev['repo_domain'] == REPO_CODE_DOMAIN == 'gitlab.com'
        assert pev['REQ_FILE_NAME'] == REQ_FILE_NAME == 'requirements.txt'
        assert pev['REQ_DEV_FILE_NAME'] == REQ_DEV_FILE_NAME == 'dev_requirements.txt'
        assert pev['project_path'] == cur_dir
        assert pev['project_type'] == 'module' == MODULE_PRJ
        assert pev['portion_name'] == module
        assert pev['version_file'] == os.path.join(cur_dir, namespace, module + PY_EXT)
        assert pev['package_name'] == namespace + '_' + module
        assert pev['pip_name'] == namespace + '-' + module.replace('_', '-')
        assert pev['import_name'] == namespace + '.' + module
        assert pev['package_version'] == code_file_version(os.path.join(cur_dir, namespace, module + PY_EXT))
        assert pev['repo_root']
        assert pev['repo_root'].endswith(namespace + REPO_GROUP_SUFFIX)
        assert pev['repo_pages']
        assert PYPI_PROJECT_ROOT in pev['pypi_url']

        assert pev['dev_require'] == []
        assert pev['docs_require'] == []
        assert pev['install_require'] == ['ae_base']
        assert pev['setup_require'] == ['ae_base']
        assert len(pev['tests_require']) == 8
        assert pev['portions_packages'] == []
        assert pev['project_packages'] == ['de']
        assert pev['project_resources'] == []

    def test_de_env_setup(self):
        namespace = 'de'
        module = 'setup_project'
        patched_group_suffix = 'group'
        cur_dir = os.getcwd()

        from setup import pev  # project_env_vars() from setup.py of this portion

        assert isinstance(pev, dict)
        assert pev['namespace_name'] == namespace
        assert pev['TEMPLATES_FOLDER'] == TEMPLATES_FOLDER == 'templates'
        assert pev['PYPI_PROJECT_ROOT'] == PYPI_PROJECT_ROOT == 'https://pypi.org/project'
        assert pev['REPO_CODE_DOMAIN'] == pev['repo_domain'] == REPO_CODE_DOMAIN == 'gitlab.com'
        assert pev['REQ_FILE_NAME'] == REQ_FILE_NAME == 'requirements.txt'
        assert pev['REQ_DEV_FILE_NAME'] == REQ_DEV_FILE_NAME == 'dev_requirements.txt'
        assert pev['project_path'] == cur_dir
        assert pev['project_type'] == 'module' == MODULE_PRJ
        assert pev['portion_name'] == module
        assert pev['version_file'] == os.path.join(cur_dir, namespace, module + PY_EXT)
        assert pev['package_name'] == namespace + '_' + module
        assert pev['pip_name'] == namespace + '-' + module.replace('_', '-')
        assert pev['import_name'] == namespace + '.' + module
        assert pev['package_version'] == code_file_version(os.path.join(cur_dir, namespace, module + PY_EXT))
        assert pev['repo_root'].endswith(namespace + patched_group_suffix)
        assert pev['repo_pages']
        assert PYPI_PROJECT_ROOT in pev['pypi_url']

        assert pev['dev_require'] == []
        assert pev['docs_require'] == []
        assert len(pev['install_require']) == 1
        assert pev['install_require'] == ['ae_base']
        assert pev['setup_require'] == ['ae_base']
        assert len(pev['tests_require']) == 8

        assert pev['portions_packages'] == []
        assert pev['project_packages'] == ['de']
        assert pev['project_resources'] == []

    def test_non_existent_env(self):
        empty_dir = os.path.join(os.getcwd(), TESTS_FOLDER, "empty_prj_dir")
        try:
            os.makedirs(empty_dir)

            pev = project_env_vars(project_path=empty_dir)
            assert isinstance(pev, dict)
            assert pev['namespace_name'] == ''
            assert pev['TEMPLATES_FOLDER'] == TEMPLATES_FOLDER == 'templates'
            assert pev['PYPI_PROJECT_ROOT'] == PYPI_PROJECT_ROOT == 'https://pypi.org/project'
            assert pev['REPO_CODE_DOMAIN'] == pev['repo_domain'] == REPO_CODE_DOMAIN == 'gitlab.com'
            assert pev['REQ_FILE_NAME'] == REQ_FILE_NAME == 'requirements.txt'
            assert pev['REQ_DEV_FILE_NAME'] == REQ_DEV_FILE_NAME == 'dev_requirements.txt'
            assert pev['project_path'] == empty_dir
            assert pev['project_type'] == NO_PRJ
            assert not pev['package_version']
            assert pev['repo_root']
            assert pev['repo_pages']

            assert pev['dev_require'] == []
            assert pev['docs_require'] == []
            assert pev['install_require'] == []
            assert pev['setup_require'] == ['ae_base', 'de_setup_project']
            assert pev['tests_require'] == []

            assert pev['portions_packages'] == []
            assert not pev['project_packages']
            assert not pev['project_resources']
        finally:
            if os.path.isdir(empty_dir):
                shutil.rmtree(empty_dir)

    def test_invalid_env_doesnt_raise(self):
        pev = project_env_vars(project_path="invalid_project_path")
        assert pev['project_type'] == NO_PRJ

    def test_empty_pev_doesnt_raise(self):
        file_name = os.path.join(TESTS_FOLDER, 'setup' + PY_EXT)
        try:
            write_file(file_name, "pev = {}")
            project_env_vars(project_path=TESTS_FOLDER)
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_invalid_pev_doesnt_raise(self):
        file_name = os.path.join(TESTS_FOLDER, 'setup' + PY_EXT)
        try:
            write_file(file_name, "")
            project_env_vars(project_path=TESTS_FOLDER)

            write_file(file_name, "pev = ''")
            project_env_vars(project_path=TESTS_FOLDER)

            write_file(file_name, "pev = str")
            project_env_vars(project_path=TESTS_FOLDER)

            write_file(file_name, "pev = []")
            project_env_vars(project_path=TESTS_FOLDER)

            write_file(file_name, "pev = [str]")
            project_env_vars(project_path=TESTS_FOLDER)
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_raise_syntax_error_in_setup_py(self):
        file_name = os.path.join(TESTS_FOLDER, 'setup' + PY_EXT)
        try:
            write_file(file_name, "- nothing * but : syntax / errors")
            with pytest.raises(SyntaxError):
                project_env_vars(project_path=TESTS_FOLDER)
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_root_project_in_docs(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        project_path = os.path.join(parent_dir, "nsn")
        docs_dir = os.path.join(project_path, DOCS_FOLDER)
        common_dir = os.path.join(project_path, TEMPLATES_FOLDER)
        sphinx_conf = os.path.join(docs_dir, 'conf.py')
        cur_dir = os.getcwd()
        try:
            os.makedirs(docs_dir)
            os.makedirs(common_dir)
            write_file(sphinx_conf, "file-content-irrelevant")

            os.chdir(docs_dir)
            pev = project_env_vars(project_path='..')   # simulate call from within sphinx conf.py file
            os.chdir(cur_dir)

            assert pev['project_path'] == norm_path(project_path)
        finally:
            os.chdir(cur_dir)   # restore if project_env_vars() call throws exception
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_root_project_package_version(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        namespace = "nsn"
        project_path = os.path.join(parent_dir, namespace)
        docs_dir = os.path.join(project_path, DOCS_FOLDER)
        tpl_dir = os.path.join(project_path, TEMPLATES_FOLDER)
        file_name = os.path.join(project_path, namespace + PY_EXT)
        package_version = "12.33.444"
        try:
            os.makedirs(docs_dir)
            os.makedirs(tpl_dir)
            write_file(file_name, f"__version__ = '{package_version}'")

            pev = project_env_vars(project_path=project_path)

            assert pev['package_version'] == package_version
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    # commented out because we don't need pev['project_modules'] for to store extra modules of package/project
    # def test_namespace_project_extra_modules(self):
    #     namespace = 'xxx'
    #     portion_name = 'tst_pkg'
    #     package_name = namespace + "_" + portion_name
    #     parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
    #     project_path = os.path.join(parent_dir, package_name)
    #     req_file_name = os.path.join(project_path, 'requirements.txt')
    #     package_path = os.path.join(project_path, namespace, portion_name)
    #     module_name = "tst_module_name.py"
    #     try:
    #         os.makedirs(package_path)
    #         write_file(req_file_name, namespace + '_' + portion_name)
    #         write_file(os.path.join(package_path, PY_INIT), "any_content = ''")
    #         write_file(os.path.join(package_path, module_name), "any_content = ''")
    #
    #         pev = project_env_vars(project_path=project_path)
    #
    #         assert pev['package_name'] == package_name
    #         assert pev['project_type'] == PACKAGE_PRJ
    #         assert os.path.splitext(module_name)[0] in pev['project_modules']
    #         assert '__init__' not in pev['project_modules']
    #     finally:
    #         if os.path.exists(parent_dir):
    #             shutil.rmtree(parent_dir)      # including req_file_name

    def test_patch_via_config_vars(self, restore_app_env):
        curr_path = os.getcwd()
        project_name = 'tst_prj'
        tst_name = 'MyName'
        package_dir = os.path.join(TESTS_FOLDER, project_name)
        ini_file_name = os.path.join(package_dir, project_name + '.ini')
        try:
            os.makedirs(package_dir)
            write_file(ini_file_name, f"[{PEV_DEFAULTS_SECTION_NAME}]\nSTK_AUTHOR = '{tst_name}'")
            PATH_PLACEHOLDERS['cwd'] = package_dir      # cwd is curr_path/prj-dir (initialized on test init)
            cae = ConsoleApp(app_name=project_name)
            pev = project_env_vars(project_path=package_dir, cae=cae)

            assert pev_str(pev, 'STK_AUTHOR') == tst_name
        finally:
            PATH_PLACEHOLDERS['cwd'] = curr_path
            if os.path.exists(package_dir):
                shutil.rmtree(package_dir)      # including req_file_name

    def test_patch_via_config_vars_with_cfg(self, restore_app_env):
        curr_path = os.getcwd()
        project_name = 'tst_prj'
        tst_name = 'MyName'
        package_dir = os.path.join(TESTS_FOLDER, project_name)
        ini_file_name = os.path.join(package_dir, project_name + '.cfg')
        try:
            os.makedirs(package_dir)
            write_file(ini_file_name, f"[{PEV_DEFAULTS_SECTION_NAME}]\nSTK_AUTHOR = '{tst_name}'")
            PATH_PLACEHOLDERS['cwd'] = package_dir      # cwd is curr_path/prj-dir (initialized on test init)
            cae = ConsoleApp(app_name=project_name)

            pev = project_env_vars(project_path=package_dir, cae=cae)

            assert pev_str(pev, 'STK_AUTHOR') == tst_name
        finally:
            PATH_PLACEHOLDERS['cwd'] = curr_path
            if os.path.exists(package_dir):
                shutil.rmtree(package_dir)      # including req_file_name

    def test_project_readme_md(self):
        project_name = 'test_prj'
        package_dir = os.path.join(TESTS_FOLDER, project_name)
        readme_file_name = os.path.join(package_dir, 'README.md')
        readme_content = "read me file content"
        try:
            os.makedirs(package_dir)
            write_file(readme_file_name, readme_content)

            pev = project_env_vars(project_path=package_dir)

            assert pev_str(pev, 'long_desc_content') == readme_content
            assert pev_str(pev, 'long_desc_type') == 'text/markdown'
        finally:
            if os.path.exists(package_dir):
                shutil.rmtree(package_dir)      # including req_file_name

    def test_project_readme_rst(self):
        project_name = 'test_prj'
        package_dir = os.path.join(TESTS_FOLDER, project_name)
        readme_file_name = os.path.join(package_dir, 'README.rst')
        readme_content = "read me file content"
        try:
            os.makedirs(package_dir)
            write_file(readme_file_name, readme_content)

            pev = project_env_vars(project_path=package_dir)

            assert pev_str(pev, 'long_desc_content') == readme_content
            assert pev_str(pev, 'long_desc_type') == 'text/x-rst'
        finally:
            if os.path.exists(package_dir):
                shutil.rmtree(package_dir)      # including req_file_name

    def test_setuptools_find_namespace_packages(self):
        assert setuptools.find_namespace_packages(include=['de']) == ['de']

    def test_setup_py_pev_patched_app_config(self, restore_app_env):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_pkg"
        project_path = os.path.join(parent_dir, package_name)
        ini_file = os.path.join(project_path, package_name + INI_EXT)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'App Config Author Name'
        cur_dir = os.getcwd()
        try:
            os.makedirs(project_path)
            write_file(ini_file, f"[{PEV_DEFAULTS_SECTION_NAME}]\n"
                                 f"STK_AUTHOR = {author}")
            write_file(setup_file, "from de.setup_project import project_env_vars\n"
                                   "pev = project_env_vars(from_setup=True)")
            PATH_PLACEHOLDERS['cwd'] = project_path
            cae = ConsoleApp(app_name=package_name)

            pev = project_env_vars(project_path=project_path, cae=cae)

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            PATH_PLACEHOLDERS['cwd'] = cur_dir
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_setup_py_pev_patched_setup_cfg(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_app"
        project_path = os.path.join(parent_dir, package_name)
        setup_cfg = os.path.join(project_path, 'setup' + CFG_EXT)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'Setup Cfg Author Name'
        cur_dir = os.getcwd()
        try:
            os.makedirs(project_path)
            write_file(setup_cfg, "[metadata]\n"
                                  f"author = {author}")
            write_file(setup_file, "from de.setup_project import project_env_vars\n"
                                   "pev = project_env_vars(from_setup=True)")

            os.chdir(project_path)
            pev = project_env_vars()

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            os.chdir(cur_dir)
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_setup_py_pev_patched_module_constant(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_pkg"
        project_path = os.path.join(parent_dir, package_name)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'Setup Py Patched Author Name'
        try:
            os.makedirs(project_path)
            write_file(setup_file, "import de.setup_project\n"
                                   f"de.setup_project.STK_AUTHOR = '{author}'\n"
                                   "pev = de.setup_project.project_env_vars(from_setup=True)")

            pev = project_env_vars(project_path=project_path)

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_setup_py_pev_patched_module_constant_with_project_path(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_pkg"
        project_path = os.path.join(parent_dir, package_name)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'Setup Py Patched Author Name'
        try:
            os.makedirs(project_path)
            write_file(setup_file, "from os.path import dirname as d\n"
                                   "import de.setup_project\n"
                                   f"de.setup_project.STK_AUTHOR = '{author}'\n"
                                   "pev = de.setup_project.project_env_vars(project_path=d(__file__), from_setup=True)")

            pev = project_env_vars(project_path=project_path)

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_setup_py_pev_patch_priority123(self, restore_app_env):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_pkg"
        project_path = os.path.join(parent_dir, package_name)
        ini_file = os.path.join(project_path, package_name + INI_EXT)
        setup_cfg = os.path.join(project_path, 'setup' + CFG_EXT)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'App Config1 Patched Author Name'
        cur_dir = os.getcwd()
        try:
            os.makedirs(project_path)
            write_file(ini_file, f"[{PEV_DEFAULTS_SECTION_NAME}]\n"
                                 f"STK_AUTHOR = {author}")
            write_file(setup_cfg, "[metadata]\n"
                                  f"author = {author} Pri2")
            write_file(setup_file, "import de.setup_project\n"
                                   f"de.setup_project.STK_AUTHOR = '{author} Pri3'\n"
                                   "pev = de.setup_project.project_env_vars(from_setup=True)")

            PATH_PLACEHOLDERS['cwd'] = project_path
            cae = ConsoleApp(app_name=package_name)

            pev = project_env_vars(project_path=project_path, cae=cae)

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            PATH_PLACEHOLDERS['cwd'] = cur_dir
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_setup_py_pev_patch_priority23(self, restore_app_env):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_pkg"
        project_path = os.path.join(parent_dir, package_name)
        setup_cfg = os.path.join(project_path, 'setup' + CFG_EXT)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'Monkey Patched Author Name'
        try:
            os.makedirs(project_path)
            write_file(setup_cfg, "[metadata]\n"
                                  f"author = {author}")
            write_file(setup_file, "import de.setup_project\n"
                                   f"de.setup_project.STK_AUTHOR = '{author} Pri3'\n"
                                   "pev = de.setup_project.project_env_vars(from_setup=True)")

            pev = project_env_vars(project_path=project_path)

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_setup_py_pev_patch_priority13(self, restore_app_env):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        package_name = "tst_pkg"
        project_path = os.path.join(parent_dir, package_name)
        ini_file = os.path.join(project_path, package_name + INI_EXT)
        setup_file = os.path.join(project_path, 'setup' + PY_EXT)
        author = 'App Config2 Patched Author Name'
        cur_dir = os.getcwd()
        try:
            os.makedirs(project_path)
            write_file(ini_file, f"[{PEV_DEFAULTS_SECTION_NAME}]\n"
                                 f"STK_AUTHOR = {author}")
            write_file(setup_file, "import de.setup_project\n"
                                   f"de.setup_project.STK_AUTHOR = '{author} Pri3'\n"
                                   "pev = de.setup_project.project_env_vars(from_setup=True)")

            PATH_PLACEHOLDERS['cwd'] = project_path
            cae = ConsoleApp(app_name=package_name)

            pev = project_env_vars(project_path=project_path, cae=cae)

            assert pev['STK_AUTHOR'] == author
            assert pev['setup_kwargs']['author'] == author
        finally:
            PATH_PLACEHOLDERS['cwd'] = cur_dir
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)


class TestProjectTypeAndResources:
    """ project type, modules and resources unit tests """
    def test_invalid_portion(self):
        project_path = "invalid_project_path"

        pev = project_env_vars(project_path=project_path)

        assert pev['project_type'] == NO_PRJ
        assert pev['project_path'] == norm_path(project_path)
        assert pev['package_name'] == project_path
        assert pev['namespace_name'] == ""
        # assert not pev['project_modules']

    def test_no_modules(self):
        prj_name = 'prj_nam'
        project_path = os.path.join(TESTS_FOLDER, prj_name)
        try:
            os.makedirs(project_path)

            pev = project_env_vars(project_path=project_path)

            assert pev['project_type'] == NO_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == prj_name
            assert pev['namespace_name'] == ""
            # assert not pev['project_modules']
        finally:
            if os.path.exists(project_path):
                shutil.rmtree(project_path)

    def test_tests_folder_with_conftest(self):
        pev = project_env_vars(project_path=TESTS_FOLDER)
        assert pev['project_type'] == NO_PRJ
        assert pev['project_path'] == norm_path(TESTS_FOLDER)
        assert pev['package_name'] == TESTS_FOLDER
        assert pev['namespace_name'] == ""
        # assert 'conftest' in pev['project_modules']
        # assert os.path.splitext(os.path.basename(__file__))[0] in pev['project_modules']

    def test_app_project(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        package_name = 'app_project'
        project_path = os.path.join(parent_dir, package_name)
        file_name = os.path.join(project_path, BUILD_CONFIG_FILE)
        try:
            os.makedirs(project_path)
            write_file(file_name, "spec content")
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == APP_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == ""
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_app_namespace_project(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        namespace_name = 'nsn'
        portion_name = 'app_project'
        package_name = namespace_name + '_' + portion_name
        project_path = os.path.join(parent_dir, package_name)
        namespace_sub_dir = os.path.join(project_path, namespace_name)
        file_name = os.path.join(project_path, BUILD_CONFIG_FILE)
        try:
            os.makedirs(namespace_sub_dir)
            write_file(file_name, "spec content")
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == APP_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == namespace_name
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_app_project_no_underscore(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        package_name = 'appName'
        project_path = os.path.join(parent_dir, package_name)
        file_name = os.path.join(project_path, BUILD_CONFIG_FILE)
        try:
            os.makedirs(project_path)
            write_file(file_name, "spec content")
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == APP_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == ""
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_module_template_project(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        package_name = 'tpl_project'
        project_path = os.path.join(parent_dir, package_name)
        template_path = os.path.join(project_path, TEMPLATES_FOLDER)
        try:
            os.makedirs(template_path)
            write_file(os.path.join(project_path, package_name + PY_EXT), "__version__ = '1.2.3'")
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == MODULE_PRJ
            assert pev['namespace_name'] == ""
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_package_template_project(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        package_name = 'tpl_project'
        project_path = os.path.join(parent_dir, package_name)
        package_path = os.path.join(project_path, package_name)
        template_path = os.path.join(project_path, TEMPLATES_FOLDER)
        try:
            os.makedirs(package_path)
            os.makedirs(template_path)
            write_file(os.path.join(package_path, PY_INIT), "__version__ = '1.2.3'")
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == PACKAGE_PRJ
            assert pev['namespace_name'] == ""
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_namespace_template_module_project(self):
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        namespace_name = 'nsn'
        portion_name = 'tpl_project'
        package_name = namespace_name + '_' + portion_name
        project_path = os.path.join(parent_dir, package_name)
        template_path = os.path.join(project_path, TEMPLATES_FOLDER)
        namespace_path = os.path.join(project_path, namespace_name)
        try:
            os.makedirs(template_path)
            os.makedirs(namespace_path)
            write_file(os.path.join(namespace_path, portion_name + PY_EXT), "__version__ = '1.2.3'")
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == MODULE_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == namespace_name
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_namespace_root_project(self):
        namespace = 'rootX'  # no_underscore
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        project_path = os.path.join(parent_dir, namespace)
        docs_path = os.path.join(project_path, DOCS_FOLDER)
        template_path = os.path.join(project_path, TEMPLATES_FOLDER)
        try:
            os.makedirs(docs_path)
            os.makedirs(template_path)
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == ROOT_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == namespace
            assert pev['namespace_name'] == namespace
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_namespace_root_project_with_namespace_sub_dir(self):
        namespace = 'namespace'
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        project_path = os.path.join(parent_dir, namespace)
        nss_dir = os.path.join(project_path, namespace)
        doc_dir = os.path.join(project_path, DOCS_FOLDER)
        tpl_dir = os.path.join(project_path, TEMPLATES_FOLDER)
        try:
            os.makedirs(nss_dir)
            write_file(os.path.join(nss_dir, namespace + PY_EXT), "__version__ = '1.2.3'")

            os.makedirs(os.path.join(nss_dir, namespace))   # simulate also package
            write_file(os.path.join(nss_dir, namespace, PY_INIT), "__version__ = '1.2.3'")

            os.makedirs(doc_dir)
            os.makedirs(tpl_dir)
            pev = project_env_vars(project_path=project_path)
            assert pev['project_type'] == ROOT_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == namespace
            assert pev['namespace_name'] == namespace
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_one_module(self):
        mod_name = pkg_name = 'tst_pkg'
        project_path = os.path.join(TESTS_FOLDER, pkg_name)
        module_path = os.path.join(project_path, mod_name + PY_EXT)
        try:
            os.makedirs(project_path)
            write_file(module_path, "v = 3")

            pev = project_env_vars(project_path=project_path)

            assert pev['project_type'] == MODULE_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == pkg_name
            assert pev['namespace_name'] == ""
            # assert pev['project_modules'] == (mod_name, )
        finally:
            if os.path.exists(project_path):
                shutil.rmtree(project_path)

    def test_one_namespace_module(self):
        namespace = 'nsn'
        mod_name = 'tst_mod1'
        pkg_name = namespace + '_' + mod_name
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0])
        project_path = os.path.join(parent_dir, pkg_name)
        por_folder = os.path.join(project_path, namespace)
        module_path = os.path.join(por_folder, mod_name + PY_EXT)
        try:
            os.makedirs(por_folder)
            write_file(module_path, "v = 3")

            pev = project_env_vars(project_path=project_path)

            assert pev['project_type'] == MODULE_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == pkg_name
            assert pev['namespace_name'] == namespace
            # assert pev['project_modules'] == (mod_name, )
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_sub_package(self):
        package_name = 'tst_sub_pkg'
        mod_name = 'module1'
        project_path = os.path.join(TESTS_FOLDER, package_name)
        tst_file1 = os.path.join(project_path, PY_INIT)
        tst_file2 = os.path.join(project_path, mod_name + PY_EXT)
        try:
            os.makedirs(project_path)
            write_file(tst_file1, "v = 3")
            write_file(tst_file2, "v = 6")

            pev = project_env_vars(project_path=project_path)

            assert pev['project_type'] == PACKAGE_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == ""
            # assert pev['project_modules'] == (mod_name, )
        finally:
            if os.path.exists(project_path):
                shutil.rmtree(project_path)

    def test_namespace_sub_package(self):
        namespace = 'namespace'
        package_name = 'tst_sub_pkg'
        mod_name = 'module1'
        parent_dir = os.path.join(TESTS_FOLDER, PARENT_FOLDERS[-1])
        project_path = os.path.join(parent_dir, namespace + '_' + package_name)
        package_root = os.path.join(project_path, namespace, package_name)
        tst_file1 = os.path.join(package_root, PY_INIT)
        tst_file2 = os.path.join(package_root, mod_name + PY_EXT)
        try:
            os.makedirs(package_root)
            write_file(tst_file1, "v = 3")
            write_file(tst_file2, "v = 6")

            pev = project_env_vars(project_path=project_path)

            assert pev['project_type'] == PACKAGE_PRJ
            assert pev['project_path'] == norm_path(project_path)
            assert pev['package_name'] == namespace + '_' + package_name
            assert pev['portion_name'] == package_name
            assert pev['namespace_name'] == namespace
            # assert pev['project_modules'] == (mod_name, )
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_two_modules_package(self):
        mod1 = 'mod1'
        mod2 = 'mod2'
        tst_init = os.path.join(TESTS_FOLDER, PY_INIT)
        tst_file1 = os.path.join(TESTS_FOLDER, mod1 + PY_EXT)
        tst_file2 = os.path.join(TESTS_FOLDER, mod2 + PY_EXT)
        try:
            write_file(tst_init, "v = 3")
            write_file(tst_file1, "v = 6")
            write_file(tst_file2, "v = 99")

            pev = project_env_vars(project_path=TESTS_FOLDER)

            assert pev['project_type'] == PACKAGE_PRJ
            assert pev['project_path'] == norm_path(TESTS_FOLDER)
            assert pev['package_name'] == TESTS_FOLDER
            assert pev['namespace_name'] == ""
            # assert len(pev['project_modules']) >= 2     # == 4 (including conftest.py and test_de_setup_project.py)
            # assert mod1 in pev['project_modules'] and mod2 in pev['project_modules']
        finally:
            if os.path.exists(tst_init):
                os.remove(tst_init)
            if os.path.exists(tst_file1):
                os.remove(tst_file1)
            if os.path.exists(tst_file2):
                os.remove(tst_file2)

    def test_two_modules_no_init(self):
        mod1 = 'mod1'
        mod2 = 'mod2'
        tst_file1 = os.path.join(TESTS_FOLDER, mod1 + PY_EXT)
        tst_file2 = os.path.join(TESTS_FOLDER, mod2 + PY_EXT)
        try:
            write_file(tst_file1, "v = 3")
            write_file(tst_file2, "v = 55")

            pev = project_env_vars(project_path=TESTS_FOLDER)

            assert pev['project_type'] == NO_PRJ
            assert pev['project_path'] == norm_path(TESTS_FOLDER)
            assert pev['package_name'] == TESTS_FOLDER
            assert pev['namespace_name'] == ""
            # assert 'conftest' in pev['project_modules']
            # assert os.path.splitext(os.path.basename(__file__))[0] in pev['project_modules']
            # assert mod1 in pev['project_modules']
            # assert mod2 in pev['project_modules']
        finally:
            if os.path.exists(tst_file1):
                os.remove(tst_file1)
            if os.path.exists(tst_file2):
                os.remove(tst_file2)

    def test_two_namespace_modules_no_init(self):
        mod1 = 'mod1'
        mod2 = 'mod2'
        tst_file1 = os.path.join(TESTS_FOLDER, mod1 + PY_EXT)
        tst_file2 = os.path.join(TESTS_FOLDER, mod2 + PY_EXT)
        try:
            write_file(tst_file1, "v = 3")
            write_file(tst_file2, "v = 55")

            pev = project_env_vars(project_path=TESTS_FOLDER)

            assert pev['project_type'] == NO_PRJ
            assert pev['project_path'] == norm_path(TESTS_FOLDER)
            assert pev['package_name'] == TESTS_FOLDER
            assert pev['namespace_name'] == ""
            # assert 'conftest' in pev['project_modules']
            # assert os.path.splitext(os.path.basename(__file__))[0] in pev['project_modules']
            # assert mod1 in pev['project_modules']
            # assert mod2 in pev['project_modules']
        finally:
            if os.path.exists(tst_file1):
                os.remove(tst_file1)
            if os.path.exists(tst_file2):
                os.remove(tst_file2)

    def test_new_project_in_parent(self):
        package_name = PARENT_FOLDERS[0]
        parent_dir = norm_path(os.path.join(TESTS_FOLDER, package_name))
        try:
            os.makedirs(parent_dir)

            pev = project_env_vars(project_path=parent_dir)

            assert pev['project_type'] == PARENT_PRJ
            assert pev['project_path'] == parent_dir
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == ""
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)

    def test_new_project_under_parent(self):
        package_name = 'new_prj'
        parent_dir = norm_path(os.path.join(TESTS_FOLDER, PARENT_FOLDERS[0]))
        project_path = os.path.join(parent_dir, package_name)
        try:
            os.makedirs(project_path)

            pev = project_env_vars(project_path=project_path)

            assert pev['project_type'] == NO_PRJ
            assert pev['project_path'] == project_path
            assert pev['package_name'] == package_name
            assert pev['namespace_name'] == ""
        finally:
            if os.path.exists(parent_dir):
                shutil.rmtree(parent_dir)


class TestSetuptoolsConfig:
    """ test setuptools.setup kwargs config. """
    def test_metadata_author_email_default(self):
        pev = project_env_vars(from_setup=True)
        dist = setuptools.setup(script_args=["--name"], **pev['setup_kwargs'])
        assert dist.get_author_email() == STK_AUTHOR_EMAIL

    def test_metadata_author_email_config(self):
        cfg_file = os.path.join(TESTS_FOLDER, 'setup.cfg')
        tst_email = 'abc@def.xyz'
        try:
            write_file(cfg_file, "[metadata]\n"
                                 f"author_email = {tst_email}")

            pev = project_env_vars(project_path=TESTS_FOLDER)

            assert pev['setup_kwargs']['author_email'] == tst_email

            pev['setup_kwargs']['version'] = "3.3.3"
            dist = setuptools.setup(script_args=["--name"], **pev['setup_kwargs'])

            assert dist.get_author_email() == tst_email

        finally:
            if os.path.exists(cfg_file):
                os.remove(cfg_file)

    def test_metadata_name_default(self):
        pev = project_env_vars(from_setup=True)
        assert pev['package_name'] == 'de_setup_project'

        dist = setuptools.setup(script_args=["--name"], **pev['setup_kwargs'])

        assert dist.get_name() == 'de_setup_project'

        pev = project_env_vars(project_path=TESTS_FOLDER)

        assert pev['package_name'] == TESTS_FOLDER

        pev['setup_kwargs']['version'] = "3.3.3"
        dist = setuptools.setup(script_args=["--name"], **pev['setup_kwargs'])

        assert dist.get_name() == TESTS_FOLDER

    def test_metadata_name_from_config(self):
        cfg_file = os.path.join(TESTS_FOLDER, 'setup.cfg')
        tst_name = 'tst_prj_nam'
        try:
            write_file(cfg_file, "[metadata]\n"
                                 f"name = {tst_name}")

            pev = project_env_vars(project_path=TESTS_FOLDER)

            assert pev['package_name'] == tst_name
            assert pev['repo_url'].endswith(tst_name)
            assert pev['setup_kwargs']['name'] == tst_name

            pev['setup_kwargs']['version'] = "3.3.3"
            dist = setuptools.setup(script_args=["--author"], **pev['setup_kwargs'])

            assert dist.get_name() == tst_name

        finally:
            if os.path.exists(cfg_file):
                os.remove(cfg_file)
